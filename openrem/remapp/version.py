__version__ = "0.8.1"                   # Actual version number
__docs_version__ = "latest"              # Should match the tag readthedocs will build against
__short_version__ = "0.8"               # Short version number for setuptools config
__skin_map_version__ = "0.7"            # To enable changes to skinmap file format
__netdicom_implementation_version__ = "0.8.1.0"  # Used as part of UID when storing DICOM objects
__repo_branch__ = "0.8.1"               # __repo_branch__feeds into construction of link in netdicom-orthanc-config doc
                                        # to orthanc example configuration on bitbucket. Should be set to tag name
                                        # at release, not branch name.
