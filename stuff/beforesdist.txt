Change the version number in 
	openrem/remapp/version.py
	install instructions in the release notes where applicable
    	quick_start_linux.rst - set to version for beta or versionless for release
	    install.rst - set to version for beta or versionless for release (two times, title and command)
	    release-0.x.x.rst - set to version (upgrades always need version specified)
	    install-offline.rst (three times)
	    upgrade-offline.rst (three times)
	date and other details in changes and CHANGES
    Edit README.rst
    Edit description in setup.py between beta and release

Clean the existing distribute folder:
	rm -r *

Then get the new contents:
rsync -av --exclude-from='../bbOpenREM/stuff/distexclude.txt' ../bbOpenREM/ .
cp ../bbOpenREM/stuff/0002_0_7_fresh_install_add_median.py.inactive openrem/remapp/migrations/

Build:
	python setup.py sdist bdist_wheel

Test upload:
    twine upload --repository-url https://test.pypi.org/legacy/ dist/*

    Go to https://test.pypi.org/project/OpenREM/ to review

Real upload:
    twine upload dist/*

    Go to https://pypi.org/project/OpenREM/ to review

    Merge release branch into master
    Tag commit with release number , ie "0.8.1b1"
    Push master to bitbucket
    Merge master into develop and upload to bitbucket
    Delete release branch

    Go to https://readthedocs.org/projects/openrem/versions/ and find the new tag and set it to public and active

    Tell everyone